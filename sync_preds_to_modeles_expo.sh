#!/usr/bin/env bash

# Sync final 1 km predictions to summer/modeles_expo/temperature_hough_1km
# Use --size-only b/c times cannot be set on summer and checksum is *very* slow
dst_dir='/media/summer_iab/modeles_expo/temperature_hough_1km'
[ ! -d $dst_dir ] && mkdir $dst_dir
rsync -n -hiPu --size-only ./1.work/predictions/mod3_*_pred_1km.fst $dst_dir
rsync -n -hiPu --size-only ./1.work/grids/grd_1km.fst $dst_dir
rsync -n -hiPu --size-only ./1.work/grids/grd_1km_pys.rds $dst_dir

# Sync final 200 m predictions to summer/modeles_expo/temperature_hough_200m
# Use --size-only b/c times cannot be set on summer and checksum is *very* slow
dst_dir='/media/summer_iab/modeles_expo/temperature_hough_200m'
[ ! -d $dst_dir ] && mkdir $dst_dir
rsync -n -hiPu --size-only ./1.work/predictions/mod5_*_pred_200m.fst $dst_dir
rsync -n -hiPu --size-only ./1.work/grids/grd_200m.fst $dst_dir
rsync -n -hiPu --size-only ./1.work/grids/grd_200m_pys.rds $dst_dir
