#
# Create mod1 by joining meteo observations to mod3
#

# Load packages, helper functions, and constants
source("init.R")

report("---")
report("Create mod1 (db to fit m1 = model that will predict 1 km Ta from LST)")


# Define functions ------------------------------------------------------------

# List all dates in the month of the passed date
dates_in_month <- function(d) {
  month_start <- format(d, "%Y-%m-01") %>% as.Date
  seq.Date(
    from = month_start,
    to = seq.Date(month_start, by = "month", length.out = 2)[2] - 1,
    by = 1
  )
}

threads_fst(ncores)

for (year in constants$model_years) {

  lst_cols <- expand.grid(constants$modis_satellites, constants$modis_overpasses, "lst") %>%
              apply(1, paste, collapse = "_")

  # Skip mod1 datasets that have already been extracted
  mod1_paths <- sub("_lst$", "", lst_cols) %>%
                paste0("mod1_", ., "_", year, ".fst") %>%
                file.path(constants$joined_dir, .)
  names(mod1_paths) <- lst_cols
  no_data_flags <- sub("\\.fst", ".nodata", mod1_paths)
  if (OVERWRITE) {
    mod1_missing <- mod1_paths
  } else {
    mod1_missing <- mod1_paths[!file.exists(mod1_paths) & !file.exists(no_data_flags)]
  }
  if (length(mod1_missing) == 0) {
    next
  }

  # Load cleaned meteorological data for the year
  paste0(year, ": loading meteo observations") %>% report
  pattern <- paste0("meteo_data_", year, "-clean\\.fst")
  year_meteo <- file.path(constants$work_dir, "meteo_obs", "clean") %>%
                list.files(full.names = TRUE, pattern = pattern) %>%
                read_fst(as.data.table = TRUE)
  rm(pattern)

  # Convert the observation locations to EPSG:2154 (Lambert-93)
  xy_lamb93 <- year_meteo[, .(longitude, latitude)] %>%
               SpatialPoints(proj4string = CRS(constants$epsg_4326)) %>%
               spTransform(constants$epsg_2154) %>%
               as.data.table()
  year_meteo[, c("x", "y") := xy_lamb93]
  rm(xy_lamb93)

  # Drop unneeded columns
  year_meteo <- year_meteo[, .(date, insee_id, latitude, longitude, x, y, station_type,
                               station_elevation, temperature_min, temperature_mean,
                               temperature_max)]

  # Create mod1 for all months and missing lst cols in parallel
  # 180 seconds, uses ~ 7 GB / thread
  paste(year, ": joining meteo data for all months in parallel") %>% report
  mod1_list <- mclapply(1:12, function(month) {

    month_start <- paste0(year, "-%02d-01") %>% sprintf(month) %>% as.Date

    # Load mod3 data for the month
    mod3_path <- format(month_start, "mod3_%Y-%m.fst") %>% file.path(constants$joined_dir, .)
    format(month_start, "%Y-%m: loading") %>% paste(basename(mod3_path)) %>% report
    mod3 <- read_fst(mod3_path, as.data.table = TRUE)

    # Subset meteorological data for the month
    format(month_start, "%Y-%m: subsetting meteo observations") %>% report
    meteo <- year_meteo[date %in% dates_in_month(month_start), ]

    # For each LST column, match meteo observations to nearby grid cells with LST
    mod1_per_lst <- lapply(names(mod1_missing), function(lst_col) {

      msg <- format(month_start, "%Y-%m:") %>%
             paste("joining meteo obs to nearest grid cell with", lst_col)
      report(msg)

      # Subset cell-days with LST and get the data needed to join meteo observations
      lst_points <- mod3[!is.na(get(lst_col)), .(date, grd_1km_id, grd_1km_x, grd_1km_y)]
      obs_points <- meteo[, .(date, insee_id, x, y)]

      # Format the data as required by aodlur::nearestbyday
      setnames(lst_points, "date", "day")
      setnames(obs_points, "date", "day")

      # Match each observation to the closest grid cell within 1.5 km that has LST for that date
      # If no cell days have LST, return an empty data frame
      if (nrow(lst_points) == 0) {
        matches <- data.table(day = character(), insee_id = integer(), grd_1km_id = character())
        matches[, day := as.Date(day)]
      } else {
        matches <- aodlur::nearestbyday(
          jointo.pts = aodlur::makepointsmatrix(obs_points, "x", "y", "insee_id"),
          joinfrom.pts = aodlur::makepointsmatrix(lst_points, "grd_1km_x", "grd_1km_y", "grd_1km_id"),
          jointo = obs_points,
          joinfrom = lst_points,
          jointovarname = "insee_id",
          joinfromvarname = "grd_1km_id",
          joinprefix = "grd_1km_id",
          valuefield = "grd_1km_id",
          knearest = 9,
          maxdistance = 1500
        )
      }
      rm(lst_points, obs_points)

      # Drop unmatched observations and unneeded columns and reset column types
      # aodlur::nearestbday coerces insee_id to character
      matches <- matches[!is.na(grd_1km_id), .(date = day, insee_id, grd_1km_id)]
      matches[, insee_id := as.integer(insee_id)]

      # Select the meteo observations and grid data that were matched
      matched_meteo <- meteo[matches, on = c("date", "insee_id")]
      matched_grid <- mod3[matches, on = c("date", "grd_1km_id")]
      rm(matches)

      # Join the matched grid data and observations
      mod1 <- matched_meteo[matched_grid, on = c("date", "insee_id", "grd_1km_id")]
      rm(matched_meteo, matched_grid)

      # Drop the other LST columns and convert date to Date
      pattern <- setdiff(lst_cols, lst_col) %>% sub("_lst$", "", .) %>% paste(collapse = "|")
      drop_cols <- names(mod1)[names(mod1) %like% pattern]
      mod1[, (drop_cols) := NULL]

      # Return the data
      mod1

    })
    names(mod1_per_lst) <- names(mod1_missing)

    # Return the list of mod1 tables
    mod1_per_lst

  }, mc.cores = ncores, mc.preschedule = FALSE)

  # Save the mod1 datasets
  for (lst_col in names(mod1_missing)) {

    # Combine the mod1 tables for each month and set keys
    mod1 <- lapply(mod1_list, extract2, lst_col) %>% rbindlist
    setkeyv(mod1, c("date", "insee_id", "grd_1km_id"))

    # Save to disk
    if (nrow(mod1) == 0) {

      paste(year, ": no mod1 data for", lst_col) %>% report
      system2("touch", no_data_flags[lst_col])

    } else {

      paste(year, ": saving mod1 for", lst_col) %>% report
      threads_fst(ncores)
      mod1_path <- mod1_paths[lst_col]
      write_fst(mod1, mod1_path, compress = 100)
      paste(year, ": ->", basename(mod1_path)) %>% report

    }

  }

  rm(mod1_list, mod1, lst_col, year_meteo)

}
rm(year, lst_cols, mod1_paths, no_data_flags, mod1_missing, dates_in_month)

report(glue("Done {paste(range(constants$model_years), collapse = '-')}"))
