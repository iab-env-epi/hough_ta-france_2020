# Source a script and continue if a "Skip" error is encountered
# This is a quick and dirty way to break out of a sourced script (e.g. if the file it creates
# already exists) without stopping the overall execution flow
source_or_skip <- function(path) {

  handler <- function(e) {

    if (conditionMessage(e) == "Skip existing") {

      report(paste("Output exists; skipping rest of", path))

    } else {

      stop(e)

    }

  }

  tryCatch(
    source(path),
    error = handler
  )

}
