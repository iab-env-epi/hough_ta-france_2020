# Create a raster stack summarizing the specified data.table column
summarize_as_raster <- function(dtable, smry_col, x_col, y_col, id_col, proj, new_proj = NULL) {

  # Construct the summary expression as a string and substitute in the specified column
  # This is ~ 3x faster than using get(col)
  expr_string <- "dtable[!is.na(.smry_col.),
                         .(
                           x = mean(.x_col.),
                           y = mean(.y_col.),
                           min = min(.smry_col.),
                           mean = mean(.smry_col.),
                           max = max(.smry_col.),
                           sd = sd(.smry_col.),
                           .N
                         ),
                         by = .(.id_col.)]"
  expr_string <- gsub("\\s+", " ", expr_string)
  expr_string <- gsub("\\.smry_col\\.", smry_col, expr_string)
  expr_string <- gsub("\\.x_col\\.", x_col, expr_string)
  expr_string <- gsub("\\.y_col\\.", y_col, expr_string)
  expr_string <- gsub("\\.id_col\\.", id_col, expr_string)

  # Evaluate the string as an expression to calculate the summary statistics
  smry <- eval(parse(text = expr_string))


  # Ensure the id column is an integer (raster layers cannot store strings)
  smry[, (id_col) := as.integer(as.factor(get(id_col)))]

  # Convert to a RasterStack in MODIS sinusoidal projection
  # Don't warn if grid contains empty columns/rows
  coordinates(smry) <- ~ x + y
  proj4string(smry) <- proj
  if (!missing(new_proj)) {
    smry <- spTransform(smry, new_proj)
  }
  withCallingHandlers(
    gridded(smry) <- TRUE,
    warning = function(w) {
      if (grepl("grid has empty column/rows in dimension \\d", w)) {
        invokeRestart("muffleWarning")
      }
    }
  )
  stack(smry)

}

# Shortcut to summarize predictions on the 1 km grid
summarize_1km_preds <- function(dtable, smry_col) {
  summarize_as_raster(dtable = dtable, smry_col = smry_col, x_col = "grd_1km_x",
                      y_col = "grd_1km_y", id_col = "grd_1km_id", proj = constants$epsg_2154,
                      new_proj = constants$proj_sinu)
}

# Shortcut to summarize predictions on the 200 m grid
summarize_200m_preds <- function(dtable, smry_col) {
  summarize_as_raster(dtable = dtable, smry_col = smry_col, x_col = "x", y_col = "y",
                      id_col = "grd_200m_id", proj = constants$epsg_3035)
}
