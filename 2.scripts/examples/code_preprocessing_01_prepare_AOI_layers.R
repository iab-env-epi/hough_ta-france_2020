library(magrittr)
library(rgdal)
library(sp)
library(rgeos)
library(raster)
library(mapview)
library(parallel)

setwd("/media/qnap/Data/Echo/NEMIA.grid.01022017")

# Read 'grid' table
grid = readRDS("/media/qnap/Data/Echo/NEMIA.grid.01022017/gridroidt_2017-02-01.rds")
grid = as.data.frame(grid)
# create spatial grid by adding lat and long
coordinates(grid) = ~ lon + lat
# define projection
proj4string(grid) = "+proj=longlat +datum=WGS84 +no_defs +ellps=WGS84 +towgs84=0,0,0"

# Save points
saveRDS(grid, "grid_pnt.rds")

# Save 1km rectangular buffers
grid_atlas = spTransform(grid, "+proj=laea +lat_0=45 +lon_0=-100 +x_0=0 +y_0=0 +a=6370997 +b=6370997 +units=m +no_defs")
square_1km = gBuffer(grid_atlas, byid = TRUE, width = 500, capStyle = "SQUARE")
square_1km = spTransform(square_1km, "+proj=longlat +datum=WGS84 +no_defs +ellps=WGS84 +towgs84=0,0,0")
saveRDS(square_1km, "grid_square_1km.rds")

square_1km = gBuffer(grid_atlas, byid = TRUE, width = 1500, capStyle = "SQUARE")
square_1km = spTransform(square_1km, "+proj=longlat +datum=WGS84 +no_defs +ellps=WGS84 +towgs84=0,0,0")
saveRDS(square_1km, "grid_square_1km.rds")

# Donut buffers
distances = c(1500, 2500, 5000, 7500)
buffers = mclapply(
  distances,
  function(i) {
    tmp = gBuffer(grid_atlas, width=i, byid = TRUE)
    spTransform(tmp, "+proj=longlat +datum=WGS84 +no_defs +ellps=WGS84 +towgs84=0,0,0")
    },
  mc.cores = 4
  )
saveRDS(buffers[[1]], "grid_buffer_3km.rds")
saveRDS(buffers[[2]], "grid_buffer_5km.rds")
saveRDS(buffers[[3]], "grid_buffer_10km.rds")
saveRDS(buffers[[4]], "grid_buffer_15km.rds")

















