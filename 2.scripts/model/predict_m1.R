#
# Generate stage 2 predictions using stage 1 models (m1)
#

# Load packages, helper functions, and constants
source("init.R")

report("---")
report("Generating stage 2 predictions (m1, mod3)")


# Setup -----------------------------------------------------------------------

threads_fst(ncores)

# Define the best models for each year (based on examination of cross-validated stage 1 results)
# Use terra data in 2000-2002 b/c full year aqua data is not available
best_models <- list(
  "2000" = c(tmin = "terra_night_lst", tmean = "terra_night_lst", tmax = "terra_day_lst"),
  "2001" = c(tmin = "terra_night_lst", tmean = "terra_night_lst", tmax = "terra_day_lst"),
  "2002" = c(tmin = "terra_night_lst", tmean = "terra_night_lst", tmax = "terra_day_lst"),
  "2003" = c(tmin = "aqua_night_lst",  tmean = "terra_night_lst", tmax = "aqua_day_lst"),
  "2004" = c(tmin = "aqua_night_lst",  tmean = "terra_night_lst", tmax = "aqua_day_lst"),
  "2005" = c(tmin = "aqua_night_lst",  tmean = "terra_night_lst", tmax = "aqua_day_lst"),
  "2006" = c(tmin = "aqua_night_lst",  tmean = "terra_night_lst", tmax = "aqua_day_lst"),
  "2007" = c(tmin = "aqua_night_lst",  tmean = "terra_night_lst", tmax = "aqua_day_lst"),
  "2008" = c(tmin = "aqua_night_lst",  tmean = "terra_night_lst", tmax = "aqua_day_lst"),
  "2009" = c(tmin = "aqua_night_lst",  tmean = "terra_night_lst", tmax = "aqua_day_lst"),
  "2010" = c(tmin = "aqua_night_lst",  tmean = "terra_night_lst", tmax = "aqua_day_lst"),
  "2011" = c(tmin = "aqua_night_lst",  tmean = "terra_night_lst", tmax = "aqua_day_lst"),
  "2012" = c(tmin = "aqua_night_lst",  tmean = "terra_night_lst", tmax = "aqua_day_lst"),
  "2013" = c(tmin = "aqua_night_lst",  tmean = "terra_night_lst", tmax = "aqua_day_lst"),
  "2014" = c(tmin = "aqua_night_lst",  tmean = "terra_night_lst", tmax = "aqua_day_lst"),
  "2015" = c(tmin = "aqua_night_lst",  tmean = "terra_night_lst", tmax = "aqua_day_lst"),
  "2016" = c(tmin = "aqua_night_lst",  tmean = "terra_night_lst", tmax = "aqua_day_lst"),
  "2017" = c(tmin = "aqua_night_lst",  tmean = "terra_night_lst", tmax = "aqua_day_lst"),
  "2018" = c(tmin = "aqua_night_lst",  tmean = "terra_night_lst", tmax = "aqua_day_lst")

)

# Predict for all cell-days using m1 ------------------------------------------

# Iterate over years
# ~ 35 min / year
for (year in names(best_models)) {

  # Skip if predictions already exist
  pred_m1_path <- paste("mod3", year, "pred_m1.fst", sep = "_") %>%
                  file.path(constants$predictions_dir, .)
  if (file.exists(pred_m1_path) && !OVERWRITE) {
    next
  }

  # Load all m1 models and their metadata
  best <- best_models[[year]]
  models <- lapply(names(best), function(t_col) {
    m1_path <- paste0("m1_", t_col, "_", best[t_col], "_", year, ".rds") %>%
               file.path(constants$models_dir, .)
    paste(year, ": loading", basename(m1_path)) %>% report
    m1 <- readRDS(m1_path)
    meta <- sub("\\.rds$", "_metadata.rds", m1_path) %>% readRDS
    list(m1 = m1, meta = meta)
  })
  names(models) <- names(best)

  # Process all months in parallel
  # ~ 19 GB / month
  paste(year, ": predicting for all months in parallel") %>% report
  mod3_paths <- paste0("mod3_", year) %>%
                list.files(constants$joined_dir, pattern = ., full.names = TRUE)
  mod3_cores <- floor(get_mem() / 20000000)
  mod3 <- mclapply(mod3_paths, function(mod3_path) {

    # Load mod3 and abbreviate column names
    # ~ 60 seconds, ~ 5.7 GB
    month <- sub("mod3_(.+)\\.fst", "\\1", basename(mod3_path))
    paste(month, ": loading", basename(mod3_path)) %>% report
    mod3 <- read_fst(mod3_path, as.data.table = TRUE)
    names(mod3) %>% shorten() %>% setnames(mod3, .)

    # Predict for each model
    # ~ 15 min, ~ 6.2 GB
    for (t_col in names(best)) {

      m1 <- models[[t_col]]$m1
      meta <- models[[t_col]]$meta

      # Select rows with LST where LST error <= 2K, convert LST to C, and scale predictor cols
      # ~ 550 MB
      paste(month, t_col, ": subsetting and scaling mod2") %>% report
      lst_err_col <- paste0(meta$lst_col, "_err")
      mod2_cols <- c("date", "grd_1km_id", "climate", meta$predictors)
      mod2 <- mod3[get(lst_err_col) %in% c("<= 1K", "<= 2K"), mod2_cols, with = FALSE]
      mod2[, (meta$lst_col) := get(meta$lst_col) - 273.15]
      scale_cols(mod2, meta$scales)

      # HACK deal with column names that may be differ from the table the model was fit on
      if (grepl("climate_type", meta$formula_string)) {
        setnames(mod2, "climate", "climate_type")
      }

      # Predict using m1 -> pred_m1
      # ~ 5 min
      if (nrow(mod2) > 0) {
        paste(month, t_col, ": predicting with m1") %>% report
        mod2[, "pred_m1" := predict(m1, newdata = mod2, allow.new.levels = TRUE, re.form = NULL)]
      } else {
        paste(month, t_col, ": no cell-days with LST; skipping") %>% report
        mod2[, "pred_m1" := 0.0] # add col for prediction (does not add data b/c table has no rows)
      }

      # Add the predictions to mod3
      pred_col <- paste0("m1_", t_col)
      paste(month, t_col, ": adding", pred_col, "to mod3") %>% report
      mod3[mod2, (pred_col) := i.pred_m1, on = c("date", "grd_1km_id")]
      rm(mod2)

    }

    # Return a subset of columns including the predictions
    idw_cols <- paste0("idw_", names(best))
    pred_cols <- paste0("m1_", names(best))
    mod3[, c("date", "grd_1km_id", "grd_1km_x", "grd_1km_y", idw_cols, pred_cols), with = FALSE]

  }, mc.cores = mod3_cores, mc.preschedule = FALSE) %>% rbindlist
  setkeyv(mod3, c("date", "grd_1km_id"))
  rm(mod3_cores)

  # Save the predictions
  # ~ 6 min, ~ 8 GB file
  paste(year, ": saving predictions") %>% report
  threads_fst(ncores)
  write_fst(mod3, pred_m1_path, compress = 100)
  paste(year, ": ->", basename(pred_m1_path)) %>% report

  rm(best, models, mod3_paths, mod3)

}

rm(pred_m1_path, year, best_models)

report(glue("Done {paste(range(constants$model_years), collapse = '-')}"))
